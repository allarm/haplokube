#!/usr/bin/env bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
KEYNAME='id_haplokube'

echo "Create ssh keys and place them in the ssh_keys directory"

ssh-keygen -t ed25519 \
    -C "Haplokube keypair" \
    -f $SCRIPTPATH/../ssh_keys/$KEYNAME \
    -N ''
