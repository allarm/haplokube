#!/usr/bin/env bash

USERNAME='dave'

sudo useradd $USERNAME -m -s /bin/bash
sudo mkdir -p /home/$USERNAME/.ssh
echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" | tee /etc/sudoers.d/$USERNAME
tee -a /home/$USERNAME/.ssh/authorized_keys < /ssh_keys/haplokube.pub
chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh
