#!/usr/bin/env bash

USERNAME='dave'
CLUSTERNAME='haplokube'
KEYNAME='id_haplokube'

sudo useradd $USERNAME -m -s /bin/bash

if [ ! -f ssh_keys/$KEYNAME ]; then
    echo "Keys not found, generating keys..."
    ssh-keygen -t ed25519 \
    -C "Haplokube keypair" \
    -f $SCRIPTPATH/../ssh_keys/$KEYNAME \
    -N ''
fi

sudo mkdir -p /home/$USERNAME/.ssh
echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" | tee /etc/sudoers.d/$USERNAME
tee -a /home/$USERNAME/.ssh/authorized_keys < /ssh_keys/$KEYNAME.pub
cp /ssh_keys/$KEYNAME /home/$USERNAME/.ssh
chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh

sudo apt-get update
sudo apt-get install -y vim ranger git

sudo apt -y install python3-pip

cd /home/$USERNAME || exit

git clone https://github.com/kubernetes-sigs/kubespray.git
cd kubespray || exit
pip3 install -r requirements.txt

chown -R $USERNAME:$USERNAME /home/$USERNAME/kubespray

cp -rfp inventory/sample inventory/$CLUSTERNAME
declare -a IPS=(192.168.200.100 192.168.200.201 192.168.200.202)
CONFIG_FILE=inventory/$CLUSTERNAME/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}

